#!/usr/bin/env node

const childProcess = require("child_process");
const pack = require("./package.json");
const path = require("path");
const os = require("os");

const arch = os.arch();
const platform = os.platform();
const version = pack.version.replace(/-.+$/, "");

const ARGV_OFFSET = 2;

const electron = path.resolve(__dirname, "dist", `electron-v${version}-${platform}-${arch}`, "electron.exe");
const args = process.argv.slice(ARGV_OFFSET);

childProcess.spawn(electron, args, { stdio: "inherit" }).on("close", (code) => process.exit(code));
