#!/usr/bin/env node

const AdmZip = require("adm-zip");
const del = require("del");
const fs = require("fs");
const os = require("os");
const pack = require("./package.json");
const path = require("path");

const arch = os.arch();
const platform = os.platform();
const version = pack.version.replace(/-.+$/, "");

function extractElectron() {
    const inputPath = path.resolve(__dirname, "binaries", `electron-v${version}-${platform}-${arch}.zip`);
    const outputPath = path.resolve(__dirname, "dist", `electron-v${version}-${platform}-${arch}`);

    if (!fs.existsSync(inputPath)) {
        throw new Error(`Electron v${version} binaries for ${platform}-${arch} are not available`);
    }

    if (fs.existsSync(outputPath)) {
        del.sync(outputPath);
    }

    new AdmZip(inputPath).extractAllTo(outputPath, true);
}

function extractFfmpeg() {
    const inputPath = path.resolve(__dirname, "binaries", `ffmpeg-v${version}-${platform}-${arch}.zip`);
    const outputPath = path.resolve(__dirname, "dist", `ffmpeg-v${version}-${platform}-${arch}`);

    if (!fs.existsSync(inputPath)) {
        throw new Error(`Ffmpeg v${version} binaries for ${platform}-${arch} are not available`);
    }

    if (fs.existsSync(outputPath)) {
        del.sync(outputPath);
    }

    new AdmZip(inputPath).extractAllTo(outputPath, true);
}

extractElectron();
extractFfmpeg();
