[![npm][npm]][npm-url]
[![node][node]][node-url]
[![license][license]][license-url]

# Electron Binaries

## Install

**Install in your local project**
```shell
npm install --save-dev @midion/electron
```

**Install globally**
```shell
npm install -g @midion/electron
```

## Usage

Please, refer to the official [electron documentation](https://electron.atom.io/docs/).

**Simple example**
```js
import { app, BrowserWindow } from "electron";
import path from "path";
import url from "url";

const DARWIN = process.platform === "darwin";

function initApp () {
    // Keep a global reference of the window object, if you don't, the window will
    // be closed automatically when the JavaScript object is garbage collected.

    if (global.mainWindow) return;

    // Create the browser window.
    global.mainWindow = new BrowserWindow({ width: 400, height: 600 });

    // Emitted when the window is closed.
    global.mainWindow.on("closed", function() {
        // Dereference the window object, usually you would store windows
        // in an array if your app supports multi windows, this is the time
        // when you should delete the corresponding element.
        global.mainWindow = null;
    });

    // and load the index.html of the app.
    global.mainWindow.loadURL(url.format({
        pathname: path.join(__dirname, "../webapp/main.html"),
        protocol: "file:",
        query: { s: 4 }, // url params here
        slashes: true
    }));


    // Open the DevTools.
    //global.mainWindow.webContents.openDevTools();
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on("ready", initApp);

// Quit when all windows are closed.
app.on("window-all-closed", function () {
    // On OS X it is common for applications and their menu bar
    // to stay active until the user quits explicitly with Cmd + Q
    !DARWIN && app.quit();
});

// On OS X it's common to re-create a window in the app when the
// dock icon is clicked and there are no other windows open.
app.on("activate", initApp);
```


## Maintainers

| [![willchb-avatar]][willchb] |
|------------------------------|
| [Willian Balmant]([willchb]) |


[npm]: https://img.shields.io/npm/v/@midion/electron.svg
[npm-url]: https://npmjs.com/package/@midion/electron

[license]: https://img.shields.io/npm/l/@midion/electron.svg
[license-url]: https://gitlab.com/midion/electron/raw/master/LICENSE.md

[node]: https://img.shields.io/badge/node-%3E%3D0.12.0%20%7C%7C%20%3E%3D4.0.0-brightgreen.svg
[node-url]: https://nodejs.org

[downloads]: https://img.shields.io/npm/dm/@midion/electron.svg?style=flat-square

[willchb]: https://github.com/willchb
[willchb-avatar]: https://avatars1.githubusercontent.com/u/16672319?v=3&s=150
